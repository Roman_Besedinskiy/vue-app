import Vue from 'vue';
import Vuex from 'vuex';

Vue.use(Vuex);


export  default new Vuex.Store({
    state: {
        isAuth: false
    },
    mutations: {
        logout (state) {
            state.isAuth = false;
        },
        login (state) {
            state.isAuth = Vue.auth.isAuthenticated();
        }
    }

});