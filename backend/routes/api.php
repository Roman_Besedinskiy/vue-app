<?php

use Illuminate\Http\Request;

Route::post('register', 'Auth\Api\ApiRegisterController@register');
Route::get('logout', 'Auth\Api\ApiLoginController@logout');
Route::middleware('api')->post('logout', 'Auth\Api\ApiLoginController@logout');

Route::middleware('auth:api')->get('/user', function (Request $request) {
    return  $request->user();
});


Route::group(['middleware' =>'auth:api'] , function (){

    Route::get('logout', 'Auth\Api\ApiLoginController@logout');
    Route::resource('products','ProductsController');


});